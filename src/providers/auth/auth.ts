import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/Observable';

import { AngularFireAuth } from 'angularfire2/auth';

@Injectable()
export class AuthProvider {

  constructor(public af: AngularFireAuth) {
    console.log('Hello AuthProvider Provider');
  }

  registerUser(credentials) {
    return Observable.create(observer => {
      this.af.auth.createUserWithEmailAndPassword(
        credentials.email, credentials.password
      ).then(authData => {
        // this.af.auth.currentUser.updatePhoneNumber(credentials.phoneNumber)
        this.af.auth.currentUser.updateProfile({
          displayName: credentials.firstName + ' ' + credentials.surname,
          photoURL: ''
        });

        observer.next(authData);
      }).catch(error => {
        console.log(error);
        observer.error(error);
      });
    });
  }
}
