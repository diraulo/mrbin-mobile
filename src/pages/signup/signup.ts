import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators, AbstractControl } from '@angular/forms';
import { TabsPage } from '../tabs/tabs';

import { AuthProvider } from '../../providers/auth/auth';

@Component({
  selector: 'page-signup',
  templateUrl: 'signup.html',
})
export class SignupPage {
  signupForm: FormGroup;
  email: AbstractControl;
  password: AbstractControl;
  passwordConfirmation: AbstractControl;
  firstName: AbstractControl;
  lastName: AbstractControl;
  phoneNumber: AbstractControl;
  error: any;

  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    private fb: FormBuilder,
    private auth: AuthProvider
  ) {
    this.signupForm = this.fb.group({
      'email': [
        '',
        Validators.compose([
          Validators.required,
          Validators.pattern(/[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&amp;'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/)
        ])
      ],

      'password': [
        '',
        Validators.compose([
          Validators.required,
          Validators.minLength(6)
        ])
      ],

      'passwordConfirmation': [],
      'phoneNumber': [],
      'firstName': [],
      'lastName': [],
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SignupPage');
  }

  register() {
    console.log('Register me!!!');
    let credentials = ({
      email: this.signupForm.controls.email.value,
      password: this.signupForm.controls.password.value,
      firstName: this.signupForm.controls.firstName.value,
      lastName: this.signupForm.controls.lastName.value,
      phoneNumber: this.signupForm.controls.phoneNumber.value
    });

    this.auth.registerUser(credentials).subscribe(registerData => {
      console.log(registerData);
      alert('User is registered and logged in.');
      this.navCtrl.setRoot(TabsPage);
    }, registerError => {
      console.log(registerError);
      if (
        registerError.code === 'auth/weak-password' ||
        registerError.code === 'auth/email-already-in-use') {
        alert(registerError.message);
      }
      this.error = registerError;
    });
  }
}
